import setuptools

import setuptools_scm  # noqa: F401
import toml  # noqa: F401

setuptools.setup(package_data={'udar': ['src/udar/resources/*']})
